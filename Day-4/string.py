# 1
print('Thirty' + ' Days' + ' Of' + ' Python')

# 2
print(' '.join(['Coding', 'For', 'All']))

# 3
company = 'Coding For All'

# 4
print(company)

# 5
print(len(company))

# 6
print(company.upper())
print(company.lower())
print(company.capitalize())
print(company.title())
print(company.swapcase())

Letter = 'Coding For All'
Slice = Letter[7:]
print(Slice)


